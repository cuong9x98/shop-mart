$(document).ready(function(){
	$("img#add_img").click(function(){
		$("input#image_list").click();
	})

	if (window.File && window.FileList && window.FileReader) {
		$("#image_list").on("change", function(e) {
			var files = e.target.files,
			filesLength = files.length;
			for (let i = 0; i < filesLength; i++) {
				var f = files[i]
				var fileReader = new FileReader();
				fileReader.onload = (function(e) {
					var file = e.target;
					$(`<div  class="list_detail_photo" style="margin: 0px 6px;display: inline-block; width: 100px; height: 100px;"><input type="text" class="custom-file-input" id="${i}" name="files_compare[]" value="${e.target.result}" readonly style="display: none;"/>
						<span class="pip">
						<img class="imageThumb" style="width:100px ; height:100px" src=" ${e.target.result}" title="file.name"/>
						<br/><span class="remove" onclick="document.getElementById('${i}').value = '' " >Xóa</span>
						</span>
						</div>`).insertAfter("#image_list");
					$(".remove").click(function(){
						$(	this).parent(".pip").remove();						                    
					});
				});
				fileReader.readAsDataURL(f);
			}
		});
	}else{
		alert("Your browser doesn't support to File API")
	}
	//Xóa
	$("span.remove").click(function(){
		var id = $(this).attr("id");
		$("div#img_"+id).remove();
	})
});