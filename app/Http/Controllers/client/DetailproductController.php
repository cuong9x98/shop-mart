<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\comment;
use App\orders;
use App\orders_detail;
use App\detail_photo;
use App\mylove;
use App\tag;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Aler;
use Illuminate\Support\Facades\URL;
class DetailproductController extends Controller
{
	//Hàm hiện thị dữ liệu trên trang chi tiết sản phẩm
	public function getDetailproduct(Request $request,$id){ 
		$detail_photo = detail_photo::where('id_prod',$id)->get();
		$product = Product::where('id',$request->id)->first();
		$comment = comment::where('id_obj',$request->id)->orderBy('created_at','ASC')->get();
		$url = URL::current();
		$tag = tag::where('id_prod',$request->id)->get();
		$tang  = ($product->view)+1;
		$product->view = $tang;
		
		$product->save();
	    return view('client.chi-tiet-sp',['detail_photo'=>$detail_photo,'product'=>$product,'comment'=>$comment,'tag'=>$tag,'url'=>$url]);
	}


	//Hàm kiểm tra khách hàng đã mua sản phẩm chưa
	public function check_user_buy($request){
		 $id_order = orders::where('id_customer',Auth()->user()->id)->get();
		 foreach($id_order as $item){
			 $id_product = orders_detail::where('order_id',$item->id)->get();
			 foreach($id_product as $value){
				if($value->id_prod!=$request->id_product){
					continue;
				}else{
					return true;	
				}
			}
		 }
		 return false;
	}

	//Hàm kiểm tra khách hàng đã đánh giá chưa
	public function check_user_comment($request){
		$id_product = comment::where('id_user',Auth()->user()->id)->get();
		
		foreach ($id_product as $value){
			if ($value->id_obj!=$request->id_product) {
				continue;
			}
			else{
				return true;
			}
			
		}
		return false;
	}

	//Hàm gửi dữ liệu đánh Giá
	public function postComment(Request $request)
	{
		 
		if(Auth::check()){
			if($this->check_user_buy($request)){
				if ($this->check_user_comment($request)) {
					return back()->with('success','Sản phẩm này bạn đã đánh giá rồi.');
				}
				else{
					$conment = comment::insert([
						'content'=>$request->content,
						'id_check'=>0,
						'id_obj'=>$request->id_product,
						'id_user'=>Auth()->user()->id,
						'created_at' =>new \DateTime(),
					]);
					$product =product::where('id',$request->id_product)->first();
					$tong = count(comment::where('id_obj',$request->id_product)->get());
					$rate =($request->rate + $product->like)/$tong;
					product::where('id',$request->id_product)->update([
						"like"=>$rate,
					]);
					
					return back()->with('success','Đánh giá thành công');
				}
			}
			else{
				return back()->with('success','Bạn chưa mua sản phẩm của chúng tôi');
			}
			
		}
		else {
			return view('client.login');
		}
		
	}
	public function add_love(Request $request)
	{
		$kiemtra = mylove::where('id_prod',$request->get('id'))->where('id_id_user',Auth()->user()->id)->get();
		if(count($kiemtra)>0){
			return "Sản phẩm này bạn đã yêu thích rồi";
		}else{
			mylove::insert([
				"id_prod"=>$request->get('id'),
				"id_id_user"=>Auth()->user()->id,
				"status"=>0,
				"created_at"=> new \DateTime(),
			]);
			return "Đã thêm 1 sản phẩm yêu thích";
		}
	}
}
