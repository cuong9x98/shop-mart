<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\district;
use App\ward;
use App\province;

class Ajax_get_addressController extends Controller
{
    public function post_Quan_huyen(Request $request){
        
        $district['district'] = district::where('_province_id',$request->get('id_TinhTP'))->get();
        
        $data = "";
            foreach ($district['district'] as $value) {
                $data .= "<option value=".$value->id.">".$value->_name."</option><br>";
            }
        $data .= "";  

        echo $data;
         
    }
    public function post_Phuong_Xa(Request $request){
        $ward['ward'] = ward::where('_district_id',$request->get('id_Quan'))->get();
        
        $data = "";
            foreach ($ward['ward'] as $value) {
                $data .= "<option value=".$value->id.">".$value->_name."</option><br>";
            }
        $data .= "";  

        echo $data;
    }
}
