<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use Carbon\Carbon;
use App\province;
use App\ward;

class Profile_2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $User = User::where('id',$id)->get();
        $province = province::all();
        $ward = ward::all();
        return view('admin.admin.validation',['user'=>$User,'province'=>$province,'ward'=>$ward]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request, $id)
    {
        $arrayAddress = array("dia_chi"=>$Request->dia_chi,"TinhTP"=>$Request->TinhTP,'QuanHuyen'=>$Request->QuanHuyen,'XaPhuong'=>$Request->XaPhuong);
        if($Request->hasFile('img_name')){
            $name_img = $Request->file('img_name')->getClientOriginalName();
            $user = DB::table('users')->where('id',$id)->update(
                [
                    'img'=>$name_img,
                    'name'=>$Request->name,
                    'email'=>$Request->email,
                    'sex'=>$Request->sex,
                    'address'=>$arrayAddress,
                    'phone'=>$Request->sdt,
                    'date'=>$Request->date,
                    'updated_at'=>Carbon::now(),
                ]
            );
            $Request->file('img_name')->move('uploads/img_user/',$name_img);
        }else{
            $user = DB::table('users')->where('id',$id)->update(
                [
                    'name'=>$Request->name,
                    'email'=>$Request->email,
                    'sex'=>$Request->sex,
                    'address'=>$arrayAddress,
                    'phone'=>$Request->sdt,
                    'date'=>$Request->date,
                    'updated_at'=>Carbon::now(),
                ]
            );
        }
        return back()->with('messages','Chỉnh sửa thành công !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
