<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\address;
use DB;
use Carbon\Carbon;
class ContactController extends Controller
{
    public function DiaChi(){
    	$address = address::all();
    	return view("admin.contact.dia_chi",['address'=>$address]);
    }

    
    public function ThemDiaChi(Request $Request){
    	DB::table('address')->insert([
    		'tieu_de'=>$Request->tieu_de,
    		'dia_chi'=>$Request->dia_chi,
    		'created_at'=>Carbon::now()
    	]);
    	return back();
    }
    public function getEdit($id){
    	$address = address::where('id',$id)->get();
    	return view("admin.contact.edit_dia_chi",['address'=>$address]);
    }

    public function postEdit(Request $Request){
    	$address = address::where('id',$Request->id)->update([
    		'tieu_de'=>$Request->tieu_de,
    		'dia_chi'=>$Request->dia_chi,
    		'updated_at'=>Carbon::now()
    	]);
    	return back();
    }

    public function destroy($id){
        address::where('id',$id)->delete();
        $address = address::all();
        return view("admin.contact.dia_chi",['address'=>$address]);
    }
    public function selectAddress($id){
        $address = address::where('id',$id)->update([
            'status'=>1
        ]);
        return redirect()->route('getAddress');
    }
}
