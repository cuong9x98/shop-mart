<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\blog;
use DB;
use Carbon\Carbon;
use App\User;

class BlogController extends Controller
{
    // thêm tin tức
    public function add()
    {
        return view('admin.blog.them_tin_tuc');
    }

    // post thêm tin tức
    public function postAdd(Request $Request)
    {
        // Nếu admin tạo thì ko cần duyệt
        $User = User::where('id',$Request->creater)->get();
        foreach ($User as $user) {
            $role = $user->role;
            $admin_confirm = $user->id;
        }
        if($role==2 || $role==3){
            $status= 1;
        }else{
            $status=0;
            $admin_confirm = User::where('role',3)->inRandomOrder()->get(); 
            foreach ($admin_confirm as $id_admin) {
              $admin_confirm = $id_admin->id;
            }
        }
        $img_blog = $Request->file('img')->getClientOriginalName();
        DB::table('blog')->insert(
            [
                'title'=>$Request->name,
                'detail'=>$Request->detail,
                'img'=>$img_blog,
                'status'=>$status,
                'admin_confirm'=>$admin_confirm,
                'creater'=>$Request->creater,
                'created_at'=>Carbon::now(),
            ]
        );
        $Request->file('img')->move('uploads/img_blog/',$img_blog);
        return back()->with('messages','Thêm thành công');
    }

    // sửa tin tức
    public function edit($id)
    {
        $blog = blog::where('id',$id)->get();
        return view('admin.blog.sua_tin_tuc',['blog'=>$blog]);
    }

    // post sửa
    public function postEdit(Request $Request, $id)
    {
        // Xóa file ảnh cũ
        // $name_img_old = DB::table('blog')->where('id',$id)->get();
        // foreach ($name_img_old as $value) {
        //     $url = '../../public/uploads/detail_photo/'.$value->img;
        //     Storage::delete($url);
        // }

        // Update 
        $img_blog = $Request->file('img')->getClientOriginalName();
        DB::table('blog')->where('id',$id)->update(
            [
                'title'=>$Request->name,
                'img'=>$img_blog,
                'detail'=>$Request->detail,
                'updated_at'=>Carbon::now(),
            ]
        );
        $Request->img->storeAs('Blog', $img_blog);
        return back()->with('messages','Sửa thành công');      
    }

    public function destroy($id)
    {
        blog::where('id',$id)->delete();
        return back();  
    }

}
