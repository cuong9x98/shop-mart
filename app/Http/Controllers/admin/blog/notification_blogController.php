<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\User;
class notification_blogController extends Controller
{
	// Kiểm tra có blog mới cho admin và editor
	public function notifyBlogAdmin(Request $Request){
		$blog_new = blog::where('admin_confirm',$Request->get('id'))->where('status',0)->where('notifi',0)->get();
		if(count($blog_new) > 0){
			foreach ($blog_new as $admin){
				$admin_confirm = $admin->admin_confirm;
			}
			$Users = User::where('id',$admin_confirm)->get();
			$letter = collect(['title'=>"Bạn có ".count($blog_new)." blog chờ duyệt"]);
			Notification::send($Users, new DatabaseNotification($letter) );
			blog::where('admin_confirm',$Request->get('id'))->where('status',0)->where('notifi',0)->update(
				[
					'notifi'=>1
				]
			);
		}
	}
	public function sendNotifiToUserBlog($id,$check){
		$blog_new = blog::where('id',$id)->get();
		if(count($blog_new) > 0){
			foreach ($blog_new as $value_blog){	
				$Users = User::where('id',$value_blog->creater)->get();
				if($check == 1){
					$letter = collect(['title'=>"Blog \"".$value_blog->title."\" đã được duyệt"]);	
					$messages = "Cửa hàng Shop-mart chúng tôi xin thông báo blog \"".$value_blog->title."\" đã được duyệt bởi admin.
					Xin trân thành cảm ơn bạn đã tin tưởng chúng tôi. Chúc bạn một ngày mới vui vẻ! ";				
				}else{
					$letter = collect(['title'=>"Blog \"".$value_blog->title."\" không được duyệt"]);	
					$messages = "Cửa hàng Shop-mart chúng tôi xin thông báo sản phẩm \"".$value_blog->title."\" không được duyệt bởi một số lí do vi phạm.
					Xin thành thật xin lỗi quý khách!";		
				}					
			}
			Notification::send($Users, new DatabaseNotification($letter) );
			blog::where('id',$id)->update(
				[
					'notifi'=>1
				]
			);
		}
		return redirect()->route('send-mail-blog-sell');
		// ,['creater'=>$Users,'messages'=>$messages]

	}

}
