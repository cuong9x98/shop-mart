<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\User;
class NotificationController extends Controller
{
    // Kiểm tra có blog mới
	public function notifyBlogAdmin(Request $Request){
		$blog_new = blog::where('admin_confirm',$Request->get('id'))->where('status',0)->where('notifi',0)->get();
		if(count($blog_new) > 0){
			foreach ($blog_new as $admin){
				$admin_confirm = $admin->admin_confirm;
			}
			$Users = User::where('id',$admin_confirm)->get();
			$letter = collect(['title'=>"Bạn có ".count($blog_new)." blog chờ duyệt"]);
			Notification::send($Users, new DatabaseNotification($letter) );
			blog::where('admin_confirm',$Request->get('id'))->where('status',0)->where('notifi',0)->update(
				[
					'notifi'=>1
				]
			);
		}
	}
}
