<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
class Blog_ctv_Controller extends Controller
{
    // blog đã thêm 
    public function TinTucDaThem($id)
    {
        $blog = blog::where('creater',$id)->where('status',1)->get();
        return view('admin.blog.tin_tuc_da_them_(ctv)',['blog'=>$blog]);
    }
    // tin tức đợi duyệt
    public function TinTucDoiDuyet($id)
    {
        $blog = blog::where('creater',$id)->where('status',0)->get();
        return view('admin.blog.tin_tuc_cho_duyet_(ctv)',['blog'=>$blog]);
    }
    
}
