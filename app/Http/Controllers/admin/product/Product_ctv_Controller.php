<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\category;
use DB;

class Product_ctv_Controller extends Controller
{
    // Hiển thị sản phẩm đã tạo
    public function SanPhamDaTao($id){
    	$product = product::where('status',1)->where('creater',$id)->get();
        return view('admin.product.san_pham_da_tao_(ctv)',['product'=>$product]);
    }
    // Hiển thị sản phẩm chờ duyệt
    public function SanPhamChoDuyet($id){
    	$product = product::where('status',0)->where('creater',$id)->get();
        return view('admin.product.san_pham_cho_duyet_(ctv)',['product'=>$product]);
    }
}
