<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\User;
use App\product;

class NotificationController extends Controller
{
    # Lấy sản phẩm mới và gửi thông báo cho người dùng "có sản phẩm mới chờ duyệt"
	public function notifyAdmin(Request $Request){	
		$product = product::where('status',0)->where('notifi',0)->where('admin_confirm',$Request->get('id'))->get();
		if(count($product) > 0){
			foreach ($product as $admin){
				$admin_confirm = $admin->admin_confirm;
			}
			$Users = User::where('id',$admin_confirm)->get();
			$letter = collect(['title'=>"Bạn có ".count($product)." sản phẩm chờ duyệt"]);
			Notification::send($Users, new DatabaseNotification($letter) );
			$product = product::where('status',0)->where('notifi',0)->update(
				[
					'notifi'=>1
				]
			);
		}
	}
	# Ấn duyệt thì gửi thông báo cho người tạo là đã duyệt hoặc không
	public function sendNotifiToUserSell(Request $Request){	
		$product = product::where('id',$Request->id)->get();	
		foreach ($product as $value){
			$creater = $value->creater;
			$Users = User::where('id',$creater)->get();
			if($Request->check == 1){
				$letter = collect(['title'=>"Sản phẩm ".$value->name." đã được duyệt!"]);
				$messages = "Cửa hàng Shop-mart chúng tôi xin thông báo sản phẩm \"".$value->name."\" đã được duyệt bởi admin.
				Xin trân thành cảm ơn bạn đã tin tưởng chúng tôi. Chúc bạn một ngày mới vui vẻ! ";				
			}else{
				$letter = collect(['title'=>"Sản phẩm ".$value->name." không được duyệt!"]);
				$messages = "Cửa hàng Shop-mart chúng tôi xin thông báo sản phẩm \"".$value->name."\" không được duyệt admin bởi một số lí do vi phạm.
				Xin thành thật xin lỗi quý khách!";		
			}
			Notification::send($Users, new DatabaseNotification($letter) );
			$product = product::where('status',0)->where('notifi',0)->update(
				[
					'notifi'=>1
				]
			);	
			return redirect()->route('send-mail-product-sell',['creater'=>$creater,'messages'=>$messages]); 
			return back();		
		}	
	}
}
