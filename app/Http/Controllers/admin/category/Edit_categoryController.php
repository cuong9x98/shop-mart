<?php

namespace App\Http\Controllers\admin\category;

use Illuminate\Support\Facades\File; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;
use DB;
use Carbon\Carbon;
class Edit_categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $Request)
    {
        $category = category::where('id',$Request->get('id'))->get();
        foreach ($category as $value) {
        	$data = array("name"=>$value->name, "img"=>$value->img, "commission"=>$value->commission);
        }
        echo json_encode($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)//, $id)
    {
    	# Người dùng  thêm ảnh mới
    	if(	$Request->img_name != null ){
    		// Xóa ảnh cũ
    		$category = DB::table('category')->where('id',$Request->id_cate)->get();
    		foreach ($category as $value){
                $url = 'C:\xampp\htdocs\shop_pm2\public\uploads\img_category\\';
                File::delete($url.$value->img);
            }

            //Cập nhật category
    		$img_cate = $Request->file('img_name')->getClientOriginalName();
	        DB::table('category')->where('id',$Request->id_cate)->update(
	    		[
	    			'name'=>$Request->name,
	                'img'=>$img_cate,
                    'commission'=>$Request->commission,
	    			'updated_at'=>Carbon::now(),
	    		]
	    	);
	    	$Request->file('img_name')->move('uploads/img_category/',$img_cate);
    	}
    	# Người dùng không thêm ảnh mới
    	else{
    		DB::table('category')->where('id',$Request->id_cate)->update(
	    		[
	    			'name'=>$Request->name,
                    'commission'=>$Request->commission,
	    			'updated_at'=>Carbon::now(),
	    		]
	    	);
    	}
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
