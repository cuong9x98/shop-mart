<?php

namespace App\Http\Controllers\admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\category;
use Carbon\Carbon;

class AdaddcateController extends Controller
{
    // show category
    public function getAdCate(){
        $category = category::all();	
        return view('admin.category.category',['category'=>$category]);
    }

    // add category 
    public function postAdCate(Request $Request){
        $img_cate = $Request->file('img_name')->getClientOriginalName();
    	DB::table('category')->insert(
    		[
    			'name'=>$Request->name,
                'img'=>$img_cate,
                'commission'=>$Request->commission,
                'creater'=>$Request->creater,
    			'created_at'=>Carbon::now(),
    		]
    	);
        $Request->file('img_name')->move('uploads/img_category/',$img_cate);
        return back();
    }
}
