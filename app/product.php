<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
	protected $table = 'product';

    protected $fillable = [ // Tăng tính bảo mật cho web. Chỉ đc truyền những cái này 
        'name',
        'price',
        'qty_product',
        'description',
        'detail',
        'img',
        'promotion',
        'id_cate',           
        'creater',
        'admin_confirm',
        'tag',
        'created_at',
        'status',
        'notifi',
        'tag',
        'updated_at'
    ];

    protected $casts = [ // Gán kiểu cho biến img thành mảng
        'tag' => 'array'
    ];
    public function myloves(){
        return $this->hasMany('App\mylove','id_prod', 'id');
    }
}
