<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table = 'comment';
    public function users(){
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
}
