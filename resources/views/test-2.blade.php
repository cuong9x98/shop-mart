<!DOCTYPE html>
<html>
<head>
  <title>aaaa</title>
</head>
<body>

<input type="file" id="image_list" name="files[]" value="" multiple />

<script type="text/javascript">
    // Begin upload images
    $(document).ready(function() {
        if (window.File && window.FileList && window.FileReader) {
            $("#image_list").on("change", function(e) {
            var files = e.target.files, // Lấy các file trong thẻ input
                filesLength = files.length; 
            for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                  var file = e.target;
                  $("<span class=\"pip\">" +
                      "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                      "<br/><span class=\"remove\">@lang('kidsnow.photos.remove_photo')</span>" +
                      "</span>").insertAfter("#image_list");
                  $(".remove").click(function(){
                      $(this).parent(".pip").remove();
                  });
                
                // Old code here
                /*$("<img></img>", {
                    class: "imageThumb",
                    src: e.target.result,
                    title: file.name + " | Click to remove"
                }).insertAfter("#files").click(function(){$(this).remove();});*/
                
                });
                fileReader.readAsDataURL(f);
            }
            });
        } else {
            alert("Your browser doesn't support to File API")
        }
    });
    // End upload images

    // Submit images
    $('#button_file').click(function () {
      $('#image_list').click();
  })
</script>
</body>
</html>


