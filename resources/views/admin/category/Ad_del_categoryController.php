<?php

namespace App\Http\Controllers\admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\category;

class Ad_del_categoryController extends Controller
{
    // delete category
    public function delAdCate($id){
    	category::where('id',$id)->delete();
    	return back();
    }
}
