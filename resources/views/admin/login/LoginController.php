<?php

namespace App\Http\Controllers\admin\login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use User;

class LoginController extends Controller
{
    public function getLogin(){
    	return view('admin.login');
    }
    public function postLogin(Request $Request){
    	$remember_me = $Request->has('remember_me') ? true : false;
    	if(Auth::attempt(['username'=>$Request->username,'password'=>$Request->password],$remember_me)){
            if(Auth::User()->role < 8)
                if(Auth::User()->role == 4)
                  return redirect()->route('SanPhamDaTao',['id'=>Auth::User()->id]);
                else if(Auth::User()->role == 5)
                  return redirect()->route('getBlogAdd',['id'=>Auth::User()->id]);
                else
                  return redirect()->route('getAdHome');
            else{
               Auth::logout();
               return redirect()->route('getLogin')->withInput()->with('errors','Không đúng tài khoản hoặc mật khẩu!');
           }
       }
       else
          return back()->withInput()->with('errors','Không đúng tài khoản hoặc mật khẩu!');
  }
}

