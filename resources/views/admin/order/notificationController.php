<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\User;
use App\orders;
use App\orders_detail;
use DB;
use Carbon\Carbon;
use App\notifications;

class notificationController extends Controller
{
	# Lấy đơn hàng mới và gửi thông báo cho admin
	public function notify_order(Request $Request){
		$orders = orders::where('status',0)->where('notifi',0)->where('id_sale',null)->get();
		$Users = User::where('id',$Request->get('id'))->get();
		if(count($orders) > 0){
			$letter = collect(['title'=>"Bạn có ".count($orders)." đơn hàng mới chờ được duyệt"]);
			Notification::send($Users, new DatabaseNotification($letter) );
		}	
		$orders = orders::where('status',0)->where('notifi',0)->update(
			[
				'notifi'=>1
			]);
	}
	
	# Lấy đơn hàng mới và gửi thông báo cho sale
	public function notify_order_sale(Request $Request){
		$orders = orders::where('status',0)->where('notifi',1)->where('id_sale',$Request->get('id'))->get();
		$Users = User::where('id',$Request->get('id'))->get();
		if(count($orders) > 0){
			$letter = collect(['title'=>"Bạn có ".count($orders)." đơn hàng mới!"]);
			Notification::send($Users, new DatabaseNotification($letter) );
		}	
		$orders = orders::where('status',0)->where('id_sale',$Request->get('id'))->where('notifi',1)->update(
			[
				'notifi'=>2
			]);
	}
	public function notify_order_keToan(Request $Request){
		$orders = orders::where('status',5)->where('notifi',2)->get();
		$Users = User::where('id',$Request->get('id'))->get();
		if(count($orders) > 0){
			$letter = collect(['title'=>"Bạn có ".count($orders)." đơn hàng mới!"]);
			Notification::send($Users, new DatabaseNotification($letter) );
		}
		foreach ($orders as $value_customer) {
			$id_customer = $value_customer->id_customer;
			$Users = User::where('id',$id_customer)->get();
			$letter = collect(['title'=>"Đơn hàng ".$value_customer->TenĐH." đã được duyệt!"]);
			Notification::send($Users, new DatabaseNotification($letter) );
		}			
		$orders = orders::where('status',5)->where('notifi',2)->update(
			[
				'notifi'=>3
			]);
	}
	#Đọc thông báo
	public function read_notification(Request $Request){
		$notifications = notifications::where('id',$Request->get('id'))->update([
			'read_at'=>Carbon::now(),
		]);
	}
}
