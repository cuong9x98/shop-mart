@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
	table#tbl_add_product{
		width: 90%;
	}
	input#detail_photo{
		width: 100px
	}
	input#add_product{
		float: right;
	}
	input#promotion{
		width: 80px;
		display: inline-block;
	}
	span#promotion{
		font-size: 20px;
		font-weight: 800;
	}
	.input_add{
		margin: 10px 0 10px 0
	}
	strong{
		padding: 20px 200px;
		color: white;
		background-color: #d76161;
		border-radius: 10px;
	}
	td{
		padding-bottom: 10px
	}
	div#list_detail_photo:first-child{
		padding-bottom: 10px
	}
	#imgInp {
		position: absolute;
		z-index: -99999;
	}
	#blah{
		border: 1px solid black
	}
</style>
@endsection('css')
@section('content')
<div class="link">
	
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-left: 65px; border-radius: 4px;padding-bottom: 50px; ">
@if(Auth::User()->role == 2 || Auth::User()->role == 3 )
<a href="{{ route('getProductSell',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm đang bán</button></a>
<a href="{{ route('getProdIndex',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm chờ duyệt</button></a>
<a href="{{ route('getListconfirmProduct',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm đã duyệt</button></a>
@endif
@if(Auth::User()->role == 4 )
<a href="{{route('SanPhamDaTao',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm đã tạo</button></a>
<a href="{{route('SanPhamChoDuyet',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm chờ duyệt</button></a>
@endif
<a href="{{ route('addAdPro' )}}"><button class="btn btn-dark">Thêm sản phẩm</button></a>
@if(Session::has('messages'))
<strong>{{Session::get('messages')}}</strong>
@endif
<form method="post" id="myform" enctype="multipart/form-data">
	@csrf
	<table id="tbl_add_product" align="center">
		<h3 id="title">Thêm sản phẩm</h3>
		<tr>
			<td>Tên </td>
			<td>
				<input type="text" required="required" placeholder="Tên...." name="name" class="input_add form-control">
			</td>
		</tr>
		<tr>
			<td>Danh mục </td>
			<td>
				<select class="input_add" id="select_cate" name="cate"> 
					<option>--- Danh mục ---</option>
					@foreach($category as $value_cate)
					<option value="{{$value_cate->id}}">{{$value_cate->name}}</option>
					@endforeach                    
				</select>
			</td>
		</tr>
		<tr>
			<td>Ảnh </td>
			<td>
				{{-- Chọn một ảnh duy nhất --}}
				<input type='file' id="imgInp" name="img_name">
				<img id="blah" src="../../public/uploads/img_category/Add_img.png" style="width: 200px; height: 200px"/>	
				<script type="text/javascript">
					function readURL(input) {
						if (input.files && input.files[0]) {
							var reader = new FileReader();	
							reader.readAsDataURL(input.files[0]); 			
							reader.onload = function(e) {
								$('#blah').attr('src', e.target.result);
							}
						} 
						$("div#value_hidden").append()
					}
					$("img#blah").click(function(){
						$("input#imgInp").click()
					})
					$("img#blah_2").click(function(){
						$("input#image_list").click()
					})
					$("#imgInp").change(function() {
						readURL(this);
					});
				</script>	            	
			</td> 
		</tr>
		<tr>
			<td>Ảnh chi tiết</td>
			<td>
				{{-- Chọn nhiều ảnh --}}
				<input type="file" id="image_list" name="files[]" value="" multiple />
				<img id="blah_2" src="../../public/uploads/img_category/Add_img.png" style="width: 100px; height: 100px"/>		
				<script type="text/javascript">
						  // Begin upload images
						  $(document).ready(function() {
						  	if (window.File && window.FileList && window.FileReader) {
						  		$("#image_list").on("change", function(e) {
						  			var files = e.target.files,
						  			filesLength = files.length;
						  			for (let i = 0; i < filesLength; i++) {
						  				var f = files[i]
						  				var fileReader = new FileReader();
						  				fileReader.onload = (function(e) {
						  					var file = e.target;
						  					$(`<div class="list_detail_photo" style="margin: 0px 6px;display: inline-block; width: 100px; height: 100px">
						  						<input type="text" class="custom-file-input" id="${i}" name="files_compare[]" value="${e.target.result}" readonly style="display: none;"/>
						  						<span class="pip">
						  						<img class="imageThumb" style="width:100px ; height:100px" src=" ${e.target.result}" title="file.name"/>
						  						<br/><span class="remove" onclick="document.getElementById('${i}').value = '' " >Xóa</span>
						  						</span>
						  						</div>`).insertAfter("#image_list");
						  					$(".remove").click(function(){
						  						$(	this).parent(".pip").remove();						                    
						  					});
						  				});
						  				fileReader.readAsDataURL(f);
						  			}
						  		});
						  	} else {
						  		alert("Your browser doesn't support to File API")
						  	}
						  });
						  // End upload images
						  // Submit images
						  $('#add_product').click(function () {
						  	$('#image_list').click();
						  })
						</script>            	
					</td>
				</tr>	        		        
				<tr>
					<td>Mô tả </td>
					<td>
						<input type="text" required="required" placeholder="Mô tả...." name="description" class="input_add form-control">
					</td>
				</tr>
				<tr>
					<td>Chi tiết </td>
					<td>
						<textarea required class="input_add ckeditor" name="detail"></textarea>
					</td>
				</tr>
				<tr>
					<td>Giá </td>
					<td>
						<input type="text" required="required" placeholder="Giá...." name="price" class="input_add form-control">
					</td>
				</tr>
				<tr>
					<td>Khuyến mãi </td>
					<td>
						<input id="promotion" min="0" max="100" type="number" required="required" name="promotion" placeholder="0" class="input_add form-control"> <span id="promotion">%</span>
					</td>
				</tr>
				<tr>
					<td>Số lượng </td>
					<td>
						<input type="number" required="required" placeholder="Số lượng...." name="qty_product" class="input_add form-control">
					</td>
				</tr>	
				<tr>
					<td>tag</td>
					<td>
						<div class="tag" style="border: 1px solid #b5abab;border-right: 1px solid white;line-height: 35px;height: 35px;display: inline-block;width: auto; "></div>
						<input type="text" id="itemID" placeholder="Nhập tag ..." style="border: 1px solid #b5abab;border-left: 1px solid white;width: 200px;display: inline-block;position: absolute;margin: 0;height: 35px; ">
						<input type="hidden" name="qty_tag" id="qty_tag" value="">
						<!-- <select id='value_tag' style="width: 200px;position: absolute;margin-top: 35px; padding: 0;height: 35px; display: none;"></select> -->
					</div>
				</div>
				<script type="text/javascript">
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');	
					var i = 0;
					// Chức năng lấy từ trong db ra những cái gợi ý
					// 	$(document).ready(function(){
					// 		$('#itemID').keyup(function(){
					// 			if($(this).val().length==0)
					// 				$("select#value_tag").css('display','none');
					// 			else{
					// 				$("select#value_tag").css('display','inline-block');
					// 			//Ajax lấy data
					// 			var string = $(this).val();
					// 			$.ajaxSetup({
					// 				headers : {
					// 					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
					// 				}
					// 			});
					// 			$.ajax({
					// 				url : "http://localhost:8888/shop-mart/public/admin/get-tag",
					// 				type : "POST",
					// 				cache: false,
					// 				data : {_token:CSRF_TOKEN,string:string},
					// 				success:function(data){
					// 					if(data == 0){
					// 						$("select#value_tag").css('display','none');
					// 					}else{
					// 						$('select#value_tag').empty();
					// 						$('select#value_tag').append(data);
					// 					}
					// 				},error:function(error){
					// 					alert("Thêm thất bại");
					// 				}
					// 			});
					// 		}									
					// 	})		
					// 	// chọn giá trị
					// 	$("select#value_tag").change(function(){										
					// 		var value_tag = $(this).val();
					// 		$('#itemID').val(value_tag);
					// 		$('select#value_tag').empty();									
					// 		$("select#value_tag").css('display','none');					
					// 	})						
					// })	
					function delete_tag(i){
						var last = $('div.qty_tag').last().attr('id');
						last = last.substring(4);
						$('input#qty_tag').attr("value",Number($('div.qty_tag').length)-Number(1));	
						$("div#tag_"+i).remove();	
						var i_next = Number(i)+Number(1);					
						for(var i = i_next; i<=last;i++){
							$("div#tag_"+i).attr('id',"tag_"+(Number(i)-Number(1))); 
							$("span#tag_"+i).attr('id',"tag_"+(Number(i)-Number(1))); 
							$("i#tag_"+i).attr('id',"tag_"+(Number(i)-Number(1))); 
							$("i#tag_"+( Number(i)-Number(1) )).attr('onclick',"delete_tag("+( Number(i)-Number(1) )+")"); 
							$("input#tag_"+i).attr('id',"tag_"+(Number(i)-Number(1)));
							$("input#tag_"+( Number(i)-Number(1) )).attr('name',"tag_"+(Number(i)-Number(1)));
						}
					}
					// Enter lấy tag
					$('#itemID').keypress(function(event) {
						if (event.keyCode == 13 || event.which == 13) {
								i = Number($('div.qty_tag').length)+Number(1);
						        event.preventDefault();  //Không cho submit from bạn có thể bỏ nều k cần
						        //Các câu lệnh Logic sẽ thực hiện ở đây 
						        var data = $(this).val().toLowerCase();
						        var qty = $('div.qty_tag').length;
						        if(qty > 0){
						        	for (var j = 1; j <= qty; j++) {
						        		var check = 0;
						        		var b = $('span#tag_'+j).text().toLowerCase();
						        		if(b==data){
						        			alert("Đã tồn tại trong tag!");
						        			check = 1;
						        		}
						        	}
						        	if(check!=1)
						        		$('div.tag').append("<div class='qty_tag' style='display:inline-block' id='tag_"+i+"'><span id='tag_"+i+"' style='border-right: 1px solid white; border: 1px solid #b5abab; margin-left: 10px;padding: 3px;'>"+data+"</span><i id='tag_"+i+"' onclick='delete_tag("+i+")' class='fa fa-trash' style='color: white;background-color: red; padding: 5px;border: 1px solid #b5abab;border-left: 1px solid white; margin-right: 10px' aria-hidden='true'></i><input type='hidden' id='tag_"+i+"' name='tag_"+i+"' value='"+data+"'>,</div>"); 
						        }else{
						        	$('div.tag').append("<div class='qty_tag' style='display:inline-block' id='tag_"+i+"'><span id='tag_"+i+"' style='border-right: 1px solid white; border: 1px solid #b5abab; margin-left: 10px;padding: 3px;'>"+data+"</span><i id='tag_"+i+"' onclick='delete_tag("+i+")' class='fa fa-trash' style='color: white;background-color: red; padding: 5px;border: 1px solid #b5abab;border-left: 1px solid white; margin-right: 10px' aria-hidden='true'></i><input type='hidden' id='tag_"+i+"' name='tag_"+i+"' value='"+data+"'>,</div>"); 
						        }
						        $('#itemID').val(""); 
						        $('input#qty_tag').attr("value",$('div.qty_tag').length);					        
						    }					    
						});									
					</script>
				</td>
			</tr>        
			<tr>
				<td colspan="2">
					<input type="hidden" name="creater" value="{{Auth::User()->id}}">
					<input type="submit" id="add_product" name="add" value="Thêm sản phẩm" class="input_add btn btn-dark">
				</td>
			</tr>
		</table>
	</form>
</div>
<div style="height: 30px"></div> 		            	
@endsection('content')