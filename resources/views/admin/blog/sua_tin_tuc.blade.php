@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
	table#tbl_add_product{
		width: 90%;
	}
	h3#title{
		text-align: center;
	}
	input#detail_photo{
		width: 100px
	}
	input#add_product{
		float: right;
	}
	div#fix{
		height: 300px
	}
	input#promotion{
		width: 80px;
		display: inline-block;
	}
	span#promotion{
		font-size: 20px;
		font-weight: 800;
	}
	.input_add{
		margin: 10px 0 10px 0
	}
	input#img{
		width: 300px;
	}
	strong{
		padding: 20px 200px;
		color: white;
		background-color: #d76161;
		border-radius: 10px;
		margin: 0 290px;
	}
	i#name_prod {
		color: red;
		font-size: 30px;
	}
</style>
@endsection('css')
@section('content')
@foreach($blog as $value_blog)
<!--heder end here-->
<div class="link">
	<a class="link" href="{{route('getBlogIndex')}}">Blog</a> > {{$value_blog->title}}
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;padding-left: 65px; border-radius: 4px;padding-bottom: 50px; ">
	@if(Auth::User()->role == 2 || Auth::User()->role == 3 )
	<a href="{{route('getBlogIndex')}}"><button class="btn btn-dark">Tin tức đăng</button></a>
	<a href="{{route('getBlogIndexAdmin',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức chờ duyệt</button></a>
	<a href="{{route('getBlogIndexAdmin_confirm',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức đã duyệt</button></a>
	@endif
	@if(Auth::User()->role == 5 )
	<a href="{{route('getBlogAdd',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức đã đăng</button></a>
	<a href="{{route('TinTucDoiDuyet',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức chờ duyệt</button></a>
	@endif
	<a href="{{route('getAddBlogIndex')}}"><button class="btn btn-dark">Thêm tin tức</button></a>
	<h3 id="title">Tin tức đã thêm</h3>
	<form method="post" enctype="multipart/form-data">
		@csrf
		@if(Session::has('messages'))
		<strong>{{Session::get('messages')}}</strong>
		@endif
		<table id="tbl_add_product" align="center">
			<h3 id="title">Sửa blog " <i id="name_prod">{{$value_blog->title}}</i> "</h3>
			<tr>
				<td>Tên </td>
				<td>
					<input type="text" required="required" placeholder="Tên...." name="name" class="input_add form-control" value="{{$value_blog->title}}">
				</td>
			</tr>	           
			<tr>
				<td>Ảnh </td>
				<td>											
					<input type="file" required="required" id="img" name="img" class="input_add form-control" >
					<!-- <img src='../../storage/app/Blog/{{$value_blog->img}}'> -->
					<span id="name_img"></span>
					<script type="text/javascript">
						$('input#img').change(function(){
							var name_img = $('input#img').val().substring(12);
							$('span#name_img').html(name_img);				    			
						});
					</script>
				</td>
			</tr>        
			<tr>
				<td>Chi tiết </td>
				<td>
					<textarea style="height: 500px" required class="input_add ckeditor" name="detail">{{$value_blog->detail}}</textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="id_user" value="{{Auth::User()->id}}">
					<input type="submit" id="add_product" name="add" value="Thêm tin" class="input_add btn btn-dark">
				</td>
			</tr>
		</table>
	</form>
	@endforeach
	<div style="height: 20px"></div> 
</div>
<div style="height: 30px"></div> 
@endsection('content')