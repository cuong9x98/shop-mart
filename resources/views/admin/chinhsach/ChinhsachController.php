<?php

namespace App\Http\Controllers\admin\ChinhSach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\chinhsach;

use DB;
class ChinhsachController extends Controller
{
    //
    public function BaoHanh(){
    	$bao_hanh = chinhsach::where('check',0)->get();
    	return view('admin.chinhsach.bao_hanh',['bao_hanh'=>$bao_hanh]);
    }
    public function postBaoHanh(Request $Request){
    	$bao_hanh = chinhsach::where('check',0)->get();
    	if(count($bao_hanh) == 0){
    		DB::table('chinhsach')->insert([
    			'name'=>"Bảo mật",
	    		'detail'=>$Request->detail,
	    		'check'=>0
	    	]);
    	}else{
	    	$bao_hanh = chinhsach::where('id',$Request->id)->update([
	    		'detail'=>$Request->detail,
	    	]);
	    }
    	return redirect()->route('getBaoHanh');
    }
    public function NangCap(){
    	$nang_cap = chinhsach::where('check',1)->get();
    	return view('admin.chinhsach.nang_cap',['nang_cap'=>$nang_cap]);
    }
    public function postNangCap(Request $Request){
    	$nang_cap = chinhsach::where('check',1)->get();
    	if(count($nang_cap) == 0){
    		DB::table('chinhsach')->insert([
    			'name'=>"Nâng cấp",
	    		'detail'=>$Request->detail,
	    		'check'=>1
	    	]);
    	}else{
	    	$nang_cap = chinhsach::where('id',$Request->id)->update([
	    		'detail'=>$Request->detail,
	    	]);
	    }
    	return redirect()->route('getNangCap');
    }
    public function CaiDat(){
    	$cai_dat = chinhsach::where('check',2)->get();
    	return view('admin.chinhsach.cai_dat',['cai_dat'=>$cai_dat]);
    }
    public function postCaiDat(Request $Request){
    	$cai_dat = chinhsach::where('check',2)->get();
    	if(count($cai_dat) == 0){
    		DB::table('chinhsach')->insert([
    			'name'=>"Cài đặt",
	    		'detail'=>$Request->detail,
	    		'check'=>2
	    	]);
    	}else{
	    	$cai_dat = chinhsach::where('id',$Request->id)->update([
	    		'detail'=>$Request->detail,
	    	]);
	    }
    	return redirect()->route('getCaiDat');
    }
    public function GiaHan(){
    	$gia_han = chinhsach::where('check',3)->get();
    	return view('admin.chinhsach.gia_han',['gia_han'=>$gia_han]);
    }
    public function postGiaHan(Request $Request){
    	$gia_han = chinhsach::where('check',3)->get();
    	if(count($gia_han) == 0){
    		DB::table('chinhsach')->insert([
    			'name'=>"Cài đặt",
	    		'detail'=>$Request->detail,
	    		'check'=>3
	    	]);
    	}else{
	    	$gia_han = chinhsach::where('id',$Request->id)->update([
	    		'detail'=>$Request->detail,
	    	]);
	    }
    	return redirect()->route('getGiaHan');
    }
}
