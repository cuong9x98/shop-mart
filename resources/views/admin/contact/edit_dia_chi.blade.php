@extends('admin.master')
@section('title','Danh mục')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách địa chỉ</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tiêu đề</td>
					<td>Địa chỉ</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@foreach($address as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->tieu_de}}</td>
						<td class="value">{{$value->dia_chi}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEdit',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="delete" href="{{route('destroyDiaChi',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectAddress',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<button class="btn btn-dark" id="btn-add">Thêm địa chỉ</button>
			<p>Sửa địa chỉ</p>
			@foreach($address as $value)
			<form method="post" action="{{route('postEdit')}}">
				@csrf
				<label>Tiêu đề</label>
				<input type="text" value="{{$value->tieu_de}}" class="form-control" name="tieu_de" required="required" placeholder="Tiêu đề ...">
				<label>Địa chỉ</label>
				<input type="text" value="{{$value->dia_chi}}" class="form-control" name="dia_chi" required="required" placeholder="Địa chỉ ...">
				<input type="hidden" name="id" value="{{$value->id}}">			
				<input id="add" type="submit" name="add" value="Sửa địa chỉ">	
			</form>
			@endforeach

		</div>
	</div>
</div>
@endsection('content')
