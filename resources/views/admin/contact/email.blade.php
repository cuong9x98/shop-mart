@extends('admin.master')
@section('title','Danh mục')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách email</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tiêu đề</td>
					<td>Email</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@if(count($email) > 0)
					@foreach($email as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->tieu_de}}</td>
						<td class="value">{{$value->email}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEditEmail',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="delete" href="{{route('destroyEmail',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectemail',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a href="{{route('selectemail',['id'=>$value->id])}}"style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5"> Không có dữ liệu</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<p>Thêm email</p>
			<form method="post" action="{{route('postEmail')}}">
				@csrf
				<label>Tiêu đề</label>
				<input type="text" class="form-control" name="tieu_de" required="required" placeholder="Tiêu đề ...">
				<label>Email</label>
				<input type="email" class="form-control" name="email" required="required" placeholder="Email ...">			
				<input id="add" type="submit" name="add" value="Thêm email">	
			</form>
		</div>
	</div>
</div>
@endsection('content')
