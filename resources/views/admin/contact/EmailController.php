<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\email;
class EmailController extends Controller
{
    //
    public function Email(){
    	// css address
		$email = email::all();
		return view("admin.contact.email",['email'=>$email]);
	}
	public function ThemEmail(Request $Request){
		DB::table('email')->insert([
			'tieu_de'=>$Request->tieu_de,
			'email'=>$Request->email,
			'created_at'=>Carbon::now()
		]);
		return back();
	}
	public function getEditEmail($id){
		$email = email::where('id',$id)->get();
		return view("admin.contact.edit_email",['email'=>$email]);
	}

	public function postEditEmail(Request $Request){
		$email = email::where('id',$Request->id)->update([
			'tieu_de'=>$Request->tieu_de,
			'email'=>$Request->email,
			'updated_at'=>Carbon::now()
		]);
		return back();
	}
	public function destroyEmail($id){
        email::where('id',$id)->delete();
        $email = email::all();
		return view("admin.contact.email",['email'=>$email]);
    }

    
    public function selectemail($id){
		$email = email::where('id',$id)->get();
		foreach ($email as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$email = email::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$email = email::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('getEmail');
	}
}
