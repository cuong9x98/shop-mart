@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!-- css address.css -->
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách địa chỉ</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tiêu đề</td>
					<td>Số điện thoại</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@if(count($phone) > 0)
					@foreach($phone as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->tieu_de}}</td>
						<td class="value">{{$value->so_dien_thoai}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEditSĐT',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="delete" href="{{route('destroySĐT',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectSĐT',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a href="{{route('selectSĐT',['id'=>$value->id])}}"style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5"> Không có dữ liệu</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<div id="title_div">
				<p id="title_div">Thêm số điện thoại</p>	
			</div>
			<form method="post" action="{{route('postSĐT')}}">
				@csrf
				<label>Tiêu đề</label>
				<input type="text" class="form-control" name="tieu_de" required="required" placeholder="Tiêu đề ...">
				<label>Số điện thoại</label>
				<input type="tel" class="form-control" name="so_dien_thoai" placeholder="Số điện thoại" pattern="^\+?(?:[0-9]??).{5,14}[0-9]$" required />			
				<input id="add" type="submit" name="add" value="Thêm số điện thoại">	
			</form>
		</div>
	</div>
</div>
@endsection('content')



