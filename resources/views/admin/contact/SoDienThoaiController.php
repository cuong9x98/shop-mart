<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\phone;
class SoDienThoaiController extends Controller
{
    //
	public function SoDienThoai(){
    	// css address
		$phone = phone::all();
		return view("admin.contact.so_dien_thoai",['phone'=>$phone]);
	}
	public function ThemSoDienThoai(Request $Request){
		DB::table('phone')->insert([
			'tieu_de'=>$Request->tieu_de,
			'so_dien_thoai'=>$Request->so_dien_thoai,
			'created_at'=>Carbon::now()
		]);
		return back();
	}
	public function getEditSĐT($id){
		$phone = phone::where('id',$id)->get();
		return view("admin.contact.edit_so_dien_thoai",['phone'=>$phone]);
	}

	public function postEditSĐT(Request $Request){
		$phone = phone::where('id',$Request->id)->update([
			'tieu_de'=>$Request->tieu_de,
			'so_dien_thoai'=>$Request->so_dien_thoai,
			'updated_at'=>Carbon::now()
		]);
		return back();
	}
	public function destroySĐT($id){
		phone::where('id',$id)->delete();
		$phone = phone::all();
		return view("admin.contact.so_dien_thoai",['phone'=>$phone]);
	}
	public function selectSĐT($id){
		$phone = phone::where('id',$id)->get();
		foreach ($phone as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$phone = phone::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$phone = phone::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('getSĐT');
	}
}
