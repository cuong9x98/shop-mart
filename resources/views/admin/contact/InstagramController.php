<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\instagram;
class InstagramController extends Controller
{
    //
    public function instagram(){
    	// css address
		$instagram = instagram::all();
		return view("admin.contact.instagram",['instagram'=>$instagram]);
	}
	public function Theminstagram(Request $Request){
		DB::table('instagram')->insert([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'created_at'=>Carbon::now()
		]);
		return back();
	}
	public function getEditinstagram($id){
		$instagram = instagram::where('id',$id)->get();
		return view("admin.contact.edit_instagram",['instagram'=>$instagram]);
	}

	public function postEditinstagram(Request $Request){
		$instagram = instagram::where('id',$Request->id)->update([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'updated_at'=>Carbon::now()
		]);
		return back();
	}
	public function destroyinstagram($id){
        instagram::where('id',$id)->delete();
        $instagram = instagram::all();
		return view("admin.contact.instagram",['instagram'=>$instagram]);
    }

    public function selectinstagram($id){
		$instagram = instagram::where('id',$id)->get();
		foreach ($instagram as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$instagram = instagram::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$instagram = instagram::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('getinstagram');
	}
}
