@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!-- css address.css -->
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách email</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tiêu đề</td>
					<td>Email</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@foreach($email as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->tieu_de}}</td>
						<td class="value">{{$value->email}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEditEmail',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="delete" href="{{route('destroyEmail',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectemail',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a href="{{route('selectemail',['id'=>$value->id])}}"style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<div id="title_div">
				<p id="title_div">Sửa email "{{$value->email}}"</p>	
				<a id="back" href="{{route('getEmail')}}">Quay lại</a>
			</div>
			<form method="post" action="{{route('postEditEmail')}}">
				@csrf
				<label>Tiêu đề</label>
				<input type="text" value="{{$value->tieu_de}}" class="form-control" name="tieu_de" required="required" placeholder="Tiêu đề ...">
				<label>Số điện email</label>
				<input type="text" value="{{$value->email}}" class="form-control" name="email" required="required" placeholder="Email ...">
				<input type="hidden" name="id" value="{{$value->id}}">			
				<input id="add" type="submit" name="add" value="Sửa email">	
			</form>
			@endforeach
		</div>
	</div>
</div>
@endsection('content')