<?php

namespace App\Http\Controllers\admin\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ward;
use App\district;
use App\province;

class Ajax_get_addressController extends Controller
{
    //get address user
    public function postaddress(Request $Request){
        
        $district['district'] = district::where('_province_id',$Request->get('id_TinhTP'))->get();
        $ward['ward'] = ward::where('_province_id',$Request->get('id_TinhTP'))->where('_district_id',$Request->get('id_QuanHuyen'))->get();

        $data['district'] = "";
            foreach ($district['district'] as $value) {
                if($value->id == $Request->get('id_QuanHuyen'))
                    $data['district'] .= "<option value=".$value->id." selected='selected'>".$value->_name."</option><br>";
                else
                    $data['district'] .= "<option value=".$value->id.">".$value->_name."</option><br>";
            }
        $data['district'] .= "";

        $data['ward'] = "";
            foreach ($ward['ward'] as $value_ward) {
                if($value_ward->id == $Request->get('id_XaPhuong'))
                    $data['ward'] .= "<option value=".$value_ward->id." selected='selected'>".$value_ward->_name."</option><br>";
                else
                    $data['ward'] .= "<option value=".$value_ward->id.">".$value_ward->_name."</option><br>";
            }
        $data['ward'] .= "";
        
        echo json_encode($data);

    }

    //get Quận huyện
    public function postQuanHuyen(Request $Request){
        $district['district'] = district::where('_province_id',$Request->get('id_TinhTP'))->get();
        $data = "";
            foreach ($district['district'] as $value) {
                $data .= "<option value=".$value->id.">".$value->_name."</option><br>";
            }
        $data .= "";  
        echo $data;
    }
    //get Xã phường
    public function postXaPhuong(Request $Request){
        $ward['ward'] = ward::where('_province_id',$Request->get('id_TinhTP'))->where('_district_id',$Request->get('id_QuanHuyen'))->get();
        $data = "";
            foreach ($ward['ward'] as $value) {
                $data .= "<option value=".$value->id.">".$value->_name."</option><br>";

            }
        $data .= "";  
        echo $data;
    }

}
