@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!--heder end here-->   	
<div>
	<div class="container">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?php $i=0 ?>
				@foreach ($banner as $value_b)
				@if($value_b->status == 1)
					@if($i==0)
						<div class="carousel-item active">
							<img src="../../public/uploads/banner/{{$value_b->img}}" style="height: 600px;"  class="d-block w-100" alt="...">
						</div>
						{{$i++}}
					@else
						<div class="carousel-item">
							<img src="../../public/uploads/banner/{{$value_b->img}}" style="height: 600px;"  class="d-block w-100" alt="...">
						</div>
					@endif
				@endif
				@endforeach
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div>
		<div class="row">
			<div class="col-md-6" style="border: 1px solid black">
				<form method="post" enctype="multipart/form-data">
					@csrf
					<img id="add_img" src="../../public/uploads/Add_img.png" style="display: inline-block;float: left;">
					<input type="file" id="image_list" name="files[]" multiple />
					<input style="margin-top: 40px" type="submit" name="add" value="Thêm ảnh">
				</form>	
			</div>
			<div class="col-md-6" style="border: 1px solid black">
				<p>Danh sách banner</p>
				@if(count($banner)>0)
				@foreach ($banner as $value)
				<div id="img_{{$value->id}}" class="col-md-3">
					@if($value->status == 1)
					<div id="confirm" style="width: 30px;height: 30px;z-index: 9999;position: absolute;background-color: green;color: white;font-weight: 900;text-align: center;border-radius: 30px;line-height: 30px;"><i class="fas fa-check"></i></div>
					@endif
					<input type="text" class="custom-file-input" id="{{ $value->id }}" name="files_compare[]" value="{{ $value->img }}" readonly style="display: none;"/>
					<span class="pip">
						<a href="{{route('selectBanner',['id'=>$value->id])}}">
							<img class="imageThumb" style="width:225px ; height:100px;" src="../../public/uploads/banner/{{$value->img}}" title="file.name"/>
						</a>
						<br/>
						<a href="{{route('deleteBanner',['id'=>$value->id])}}"><span class="remove" id="{{$value->id}}" onclick="document.getElementById('{{ $value->id }}').value = '' " >Xóa</span></a>
					</span>
				</div>
				@endforeach
				@else
				<p>ko có banner</p>
				@endif
			</div>
		</div>	
	</div>
</div>
@endsection('content')
