@extends('admin.master')
@section('title','Thông tin')
@section('css')
<style type="text/css">
    img#blah {
      width: 200px;
      height: 200px;
      border-radius: 200px;
      border: 3px solid gray;
    }
    div#img_user {
     width: 200px;
      height: auto;
      margin: auto;
      margin-bottom: 25px;
    }
    input#imgInp{
      width: 100%;
      margin-top: 15px;
    }
    label.control-label {
      font-size: 26px;
    }
    input.radio{
      display: inline-block;
      width: 20px;
      height: 20px;
      margin: 0 20px 0 0;
    }
    input.radio:last-child{
      margin: 0 20px 0 20px;
    }
    input.address{
      width: 100px
    }
    .padding{
      padding-left: 20px;
    }
    .select{ 
      height: 35px;
      padding: 0;
      margin-top: 10px;
      border: 1px solid #E9E9E9;
    }

</style>
@endsection('css')
@section('content')
<div class="link">
  <a href="{{route('getProfile',['id'=>Auth::User()->id])}}" class="link">Thông tin cá nhân</a>
</div>
<div class="validation-system">
  <div class="validation-form">
    <!---->  
        @if(Session::has('messages'))
          <strong>{{Session::get('messages')}}</strong>
        @endif  
        <form method="post" enctype="multipart/form-data">
          @csrf
          @foreach ($user as $value)
            <div class="vali-form">
              <h3 id="title" style="margin-bottom:50px">Thông tin cá nhân</h3>
              <div id="img_user">
                @if($value->img!=NULL)
                  <img id="blah" src="../../public/uploads/img_user/{{ $value->img }}" alt=""> 
                @else
                  <img id="blah" src="../../public/uploads/img_user/unnamed.jpg" alt=""> 
                @endif

                <input type='file' id="imgInp" name="img_name">
                <script type="text/javascript">
                      function readURL(input) {
                        if (input.files && input.files[0]) {
                          var reader = new FileReader();  
                        reader.readAsDataURL(input.files[0]);       
                          reader.onload = function(e) {
                            $('#blah').attr('src', e.target.result);
                          }
                        } 
                        $("div#value_hidden").append()
                      }
                      $("#imgInp").change(function() {
                        readURL(this);
                      });
                </script>
              </div>
              <div class="col-md-6 form-group1" id="name">
                <label class="control-label">Name</label><br>
                <input class="change" type="text" name="name" id="name" required="" value="{{ $value->name}}" style="width: 400px">
                <span class="edit" id="name"><i class="fas fa-pencil-alt"></i></span>
              </div>
              <div class="clearfix"> </div>
            
              
              <div class="col-md-12 form-group1 group-mail" id="email">
                <label class="control-label">Email</label><br>
                <input class="change" name="email" type="text" id="email" required="" value="{{ $value->email}}" style="width: 400px">
                <span class="edit" id="email"><i class="fas fa-pencil-alt"></i></span>
              </div>
   
              <div class="clearfix"> </div>
               <div class="clearfix"> </div>
            
              
              <div class="col-md-12 form-group1 group-mail" id="email">
                <label class="control-label">Giới tính</label><br>
                <table id="radio">
                    <tr>
                      @if($value->sex!=NULL)
                        <td><input class="radio" name="sex"  type="radio" id="sex" required="" @if($value->sex==1) checked="checked" @endif value="1">Nam</td>
                        <td><input class="radio" name="sex" type="radio" id="sex" required="" @if($value->sex==0) checked="checked" @endif value="0">Nữ</td>
                      @else
                        <td><input class="radio" name="sex" type="radio" id="sex" required="" value="1">Nam</td>
                        <td><input class="radio" name="sex" type="radio" id="sex" required="" value="0">Nữ</td>
                      @endif
                    </tr>
                </table>                
              </div>

             <form method="post" enctype="multipart/form-data">
              @csrf
              {{-- Địa chỉ --}}
              <div class="clearfix"> </div>
                 <div class="col-md-6 form-group1" id="address">
                  <label class="control-label">Địa chỉ</label><br>
                  <table>
                    <thead>
                      <tr>
                        <th>Địa chỉ</th>
                        <th class="padding">Tỉnh, thành phố</th>
                        <th class="padding">Quận, huyện</th>
                        <th class="padding">Xã, phường, thị trấn</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        @if($value->address != null)
                            <td>
                              <input style="width: 200px" class="change select" name="dia_chi" type="input" id="address" placeholder="Phone Number" required="" value="{{ $value->address['dia_chi'] }}" >
                              </td>
                              <td class="padding">
                                <select style="width: 200px" class="change select" id="TinhTP" name="TinhTP" placeholder="Tỉnh, thành phố">
                                  <option selected="selected">-- Tỉnh, thành phố --</option>                              
                                  @foreach ($province as $value_province)
                                    @if($value->address['TinhTP'] == $value_province->id )
                                      <option value="{{ $value_province->id }}" selected="selected">{{ $value_province->_name }}</option>
                                    @else
                                      <option value="{{ $value_province->id }}" >{{ $value_province->_name }}</option>
                                    @endif
                                  @endforeach
                                </select>
                            </td>
                            <td class="padding">
                              <select style="width: 200px" class="change select" id="QuanHuyen" name="QuanHuyen" placeholder="Quận, huyện">
                                <option selected="selected">-- Quận, huyện --</option>
                              </select>
                            </td>
                            <td class="padding">
                              <select style="width: 200px" class="change select" id="XaPhuong" name="XaPhuong" placeholder="Xã, phường">
                                <option>-- Xã, phường --</option>
                              </select>
                            </td>
                            <td >
                               <span class="edit" id="address"><i class="fas fa-pencil-alt"></i></span>  
                            </td>
                        @else
                            <td>
                              <input style="width: 200px" class="change select" name="dia_chi" type="input" id="address" placeholder="Phone Number" required="" value="" >
                              </td>
                              <td class="padding">
                                <select style="width: 200px" class="change select" id="TinhTP" name="TinhTP" placeholder="Tỉnh, thành phố">
                                  <option selected="selected">-- Tỉnh, thành phố --</option>                              
                                  @foreach ($province as $value_province)                                    
                                      <option value="{{ $value_province->id }}" >{{ $value_province->_name }}</option>                                 
                                  @endforeach
                                </select>
                            </td>
                            <td class="padding">
                              <select style="width: 200px" class="change select" id="QuanHuyen" name="QuanHuyen" placeholder="Quận, huyện">
                                <option selected="selected">-- Quận, huyện --</option>
                              </select>
                            </td>
                            <td class="padding">
                              <select style="width: 200px" class="change select" id="XaPhuong" name="XaPhuong" placeholder="Xã, phường">
                                <option>-- Xã, phường --</option>
                              </select>
                            </td>
                            <td >
                               <span class="edit" id="address"><i class="fas fa-pencil-alt"></i></span>  
                            </td>
                        @endif
                         

                      </tr> 
                    </tbody>
                  </table>
                   
                 
                </div>
              <div class="clearfix"> </div>
              {{-- end địa chỉ --}}
         
              <div class="vali-form">              
                <div class="col-md-6 form-group1 form-last" id="phone">
                  <label class="control-label">Số điện thoại</label><br>
                  <input name="sdt" class="change" type="text" id="phone" placeholder="Mobile Number" required="" style="width: 500px" value="{{ $value->phone }}">
                  <span class="edit" id="phone"><i class="fas fa-pencil-alt"></i></span>
                </div>
                <div class="clearfix"> </div>
              </div>
  
             
              <div class="col-md-12 form-group1 group-mail">
                <label class="control-label ">Ngày sinh</label><br>
                <input name="date" style="width: 300px;" id="date" type="date" value="{{ $value->date }}" class="form-control1 ng-invalid ng-invalid-required" ng-model="model.date" required="">
                <span class="edit" id="phone"><i class="fas fa-pencil-alt"></i></span>
              </div>            
              <div class="clearfix"> </div>
              <div class="col-md-12 form-group">
                <button type="submit" id="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            <div class="clearfix"> </div>
          @endforeach
        </form>
  <!---->
 </div>
</div>
  <!--//grid-->
  <script type="text/javascript">
    $(document).ready(function(){
      $(".edit").click(function(){
        $("select#country").css('border','');
        $('input.change').attr('type','text').css('border','');
        $('input#date').css('border','');
        var id = $(this).attr("id");
        $('input#'+id).attr('type','input').css('padding','10px').css('border','2px solid red');
      });

      $("input#date").click(function(){
        $("select#country").css('border','');
        $('input.change').attr('type','text').css('border','');
        $(this).css('border','2px solid red');
      });

      $("select#country").click(function(){
        $('input.change').attr('type','text').css('border','');
        $("input#date").css('border','');
        $(this).css('border','2px solid red');
      });
    });
  </script> 

  <script type="text/javascript">
                              var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                              $(document).ready(function(){ 

                                @if($value->address != null)
                                  var id_TinhTP = {{ $value->address['TinhTP'] }};
                                  var id_QuanHuyen = {{ $value->address['QuanHuyen'] }};
                                  var id_XaPhuong = {{ $value->address['XaPhuong'] }};
                                  // show address user
                                  if( id_TinhTP != null ){
                                    $.ajaxSetup({
                                      headers : {
                                        'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
                                      }
                                    });
                                    $.ajax({
                                      url : "http://localhost:8888/shop-mart/public/admin/postaddress",
                                      type : "POST",
                                      dataType: "json",
                                      cache: false,
                                      data : {_token:CSRF_TOKEN,id_TinhTP:id_TinhTP,id_QuanHuyen:id_QuanHuyen,id_XaPhuong:id_XaPhuong},
                                      success:function(data){
                                      
                                        $("select#QuanHuyen").append(data['district']);
                                        $("select#XaPhuong").append(data['ward']);
                                      },error:function(error){
                                            alert("Thêm thất bại");
                                          }
                                      });
                                    }
                                @endif  

                                //Show Quận huyện
                                $("select#TinhTP").change(function(){
                                  var id_TinhTP = $('select#TinhTP').val();
                                    $.ajaxSetup({
                                        headers : {
                                          'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
                                        }
                                      });
                                    $.ajax({
                                        url : "http://localhost:8888/shop-mart/public/admin/postQuanHuyen",
                                        type : "POST",
                                        cache: false,
                                        data : {_token:CSRF_TOKEN,id_TinhTP:id_TinhTP},
                                        success:function(data){
                                          $("select#QuanHuyen").empty();
                                          $("select#QuanHuyen").append(data);
                                        },error:function(error){
                                          alert("Thêm thất bại");
                                        }
                                    });
                                });

                                //Show xã phường 
                                $("select#QuanHuyen").change(function(){
                                var id_TinhTP = $('select#TinhTP').val();
                                var id_QuanHuyen = $('select#QuanHuyen').val();
                                  $.ajaxSetup({
                                      headers : {
                                        'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
                                      }
                                    });
                                  $.ajax({
                                      url : "http://localhost:8888/shop-mart/public/admin/postXaPhuong",
                                      type : "POST",
                                      cache: false,
                                      data : {_token:CSRF_TOKEN,id_TinhTP:id_TinhTP,id_QuanHuyen:id_QuanHuyen},
                                      success:function(data){
                                        $("select#XaPhuong").empty();
                                        $("select#XaPhuong").append(data);
                                      },error:function(error){
                                        alert("Thêm thất bại");
                                      }
                                  });
                                });
                            });
                                
                </script>  
@endsection('content')
