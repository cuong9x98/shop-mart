@extends('client.master')
@section('title','Home')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
    .shop-tb {
        overflow: auto;
    }

    table {
        text-align: center;
        width: 100%;
    }

    th {
        min-width: 150px;
        padding: 10px 0;
    }

    th button {
        background: black;
        color: white;
        padding: 5px 10px;
        outline: none;
    }

    .table-p {
        border: 1px solid;
        padding: 10px 10px;
    }

</style>
<style>
    /* ========= ******* Increment and Decerement Input Type Number ========= ******* */

    .product-quantity>* {
        width: 36.9px;
    }

    .product-quantity>input[type="number"]::-webkit-inner-spin-button,
    .product-quantity>input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .product-quantity>input {
        border: none;
        text-align: center;
        font-size: 12px;
    }

    .product-quantity {
        display: inline-flex;
        border: 1px solid #e6e6e6;
        align-items: center;
        height: 40px;
        border-radius: 4px;
    }

    .product-quantity-plus:before,
    .product-quantity-minus:before {
        width: 11px;
        display: block;
        margin: 0 auto;
    }

    .product-quantity-plus:before {
        content: "+";
    }

    .product-quantity-minus:before {
        content: "-";
    }

    .product-quantity-plus,
    .product-quantity-minus {
        cursor: pointer;
    }

    .product-quantity-plus:before,
    .product-quantity-minus:before {
        width: 11px;
        display: block;
        margin: 0 auto;
    }

    th {
        text-align: center;
    }

    .top__menu {
        margin-bottom: 0;
    }

</style>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<?php 
    $content = Cart::content();
    $count = Cart::count();
?>
<h2 style="text-align: center;">Sản phẩm yêu thích</h2>
<hr>
<div class="container">
    <div class="shop-tb mt-5">
        <table border="1" id="cart-items">
            <tr>
                <th>ID</th>
                <th>Tên của sản phẩm</th>
                <th>Ảnh sản phẩm</th>
                <th>Mô tả</th>
                <th>Giá</th>
                <th>Đưa vào giỏ</th>
                <th>Xóa</th>
            </tr>
            <form action="" method="post">
                @csrf
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <tbody class="row-cart">
                    @foreach($data as $result)
                        @php
                            $product = App\product::where('id',$result->id_prod)->first();
                        @endphp
                    <tr id="{{$result->id}}" class="{{$result->id}}">
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</></td>
                        <td><img src="../../public/uploads/img_product/{{$product->img}}" width="80px" height="80px" alt=""></td>
                        <td>
                            {{$product->description}}
                        </td>
                        <td>
                            {{number_format($product->price - ($product->price*($product->promotion/100)),0,'.',',')}}
                        </td>
                        <td>
                            <i class="update_mylove" id="{{$result->id}}" data-id="{{$product->id}}" ><a id="{{$product->id}}" style='font-size:36px' class='fas fa-cart-plus'></a></i>
                        </td>
                        <th style="text-align:center;font-size:36px"  id="{{$result->id}}" class="delete_row"
                            onclick="return delete_product(this);"><a class="glyphicon glyphicon-trash"></a></th>
                    </tr>
                    @endforeach
                </tbody>
            </form>
        </table>
    </div>


    <div class="text-center mt-5 mb-5">
       
        <button type="button" class="btn btn-primary"><a style="color:white" href="{{route('getHome')}}">Về Trang Chủ</a></button>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" />
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" />
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />

<script> 
$(document).ready(function(){
    $( ".delete_row").click(function() {
        var row_id = (this.id);
        $.ajax({ 
            url: '{{route('xoa_mylove')}}',
            type: 'GET',
            data: {row_id: row_id},
            success: function (data) {
                $('tbody').find("tr."+row_id).remove();
                alertify.success(data);     
            }
        });
    });

    $( ".update_mylove").click(function() {
        var row_id = (this.id);
        var id = $(this).data("id");
        
        $.ajax({ 
            url: '{{route('update_mylove')}}',
            type: 'GET',
            data: {
                row_id: row_id,
                id: id,
            },
            success: function (data) {
                $('tbody').find("tr."+row_id).remove();
                $('#count').html(data['count']);
                alertify.success(data['string']);     
            }
        });
    });
});
</script>

@endsection('content')
@section('scripts')

@endsection
