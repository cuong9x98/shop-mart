@extends('client.master')
@section('title','Home')
@section('content')
    <!-- content web -->
    <div class="container-fluid">
        <div class="container">
            <div class="row mt-20">
                <!-- left layout -->
                <div class="col-md-4 sidebar left-layout">
                     
                        <h3 class="sidebar__title pt-20">
                           <b> Những chính sách của chúng tôi</b>
                        </h3>
                        <br>
                        <br>
                        <br>
                        <br>
                        <ul class="ct__blog--box">
                            <li class="ct__blog--item">
                                <p id="0" href="" class="baohanh">
                                   Bảo hành
                                </p>
                            </li>
                            <li class="ct__blog--item">
                                <p id="1" class="caidat">
                                    Cài đặt
                                </p>
                            </li>
                            <li class="ct__blog--item">
                                <p id="2" class="nangcap">
                                    Nâng cấp
                                </p>
                            </li>
                            <li class="ct__blog--item">
                                <p href="" class="ct__blog--link">
                                    Gia hạn
                                </p>
                            </li>
                        </ul>
                    </div>
                    <!-- right layout -->
                <div class="col-md-8 article right-layout" >
                    <p class="document"></p>
                </div>
                
            </div>
        </div>
    </div>
<script src="assets/js/jquery-3.5.1.min.js"></script>
<script>
    $("p.baohanh").click(function() {
        var id = (this.id);
        $.ajax({ 
            url: '{{route('baohanh')}}',
            type: 'GET',
            data: {
                id: id,
            },
            success: function (data) {
                $('p.document').html(data);
            }
        });
    });

    $("p.caidat").click(function() {
        var id = (this.id);
        $.ajax({ 
            url: '{{route('caidat')}}',
            type: 'GET',
            data: {
                id: id,
            },
            success: function (data) {
                $('p.document').html(data);
            }
        });
    });

    $("p.nangcap").click(function() {
        var id = (this.id);
        $.ajax({ 
            url: '{{route('nangcap')}}',
            type: 'GET',
            data: {
                id: id,
            },
            success: function (data) {
                $('p.document').html(data);
            }
        });
    });
</script>
@endsection('content')