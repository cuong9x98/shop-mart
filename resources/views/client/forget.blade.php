@extends('client.master')
@section('title','Đăng nhập')
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- Style-CSS -->
<link href="css/font-awesome.css" rel="stylesheet"><!-- font-awesome-icons -->
@section('content')
<section class="signin-form">
    <div class="container-fluid">
        <div class="wrapper">
            <div class="logo text-center top-bottom-gap">
               
                <!-- if logo is image enable this   
			<a class="brand-logo" href="#index.html">
			    <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
			</a> -->
            </div>
            <div class="form34">
                <h4 class="form-head">Quên mật khẩu</h4>
               
                
                <div class="form-34or">
                    <span class="pros">
                        <span>or</span>
                    </span>
                </div>
                <form action="{{route('getpassword')}}" method="post">
                @csrf
                    
                    <div class="">
                        <p class="text-head">Nhập email của bạn</p>
                        <input type="text" name="send" class="input" placeholder="" required="" />
                    </div>
                   
                    <button type="submit" class="btn btn-success btn-block">Gửi</button>
                    
                </form>
            </div>
        </div>

    </div>
</section>
@endsection
