<!-- @extends('client.master')
@section('title','Home')


@section('css')
<link rel="stylesheet" href="slick/slick/slick.css">
<link rel="stylesheet" href="slick/slick/slick-theme.css">
<style type="text/css">
    .avatar_cate {
        max-width: 100%;
        height: 150px;
        width: 170px;
        padding: 10px;
    }

    .img{
        position: relative;
    }
    .over-btn{
        position: absolute; 
        bottom: 0; 
        color: #f1f1f1; 
        width: 100%;
        transition: .5s ease;
        opacity:0;
        color: white;
        font-size: 20px;
        padding: 20px;
        text-align: center;
    }
    .img:hover .over-btn{
        opacity: 1;
    }

</style>
@endsection('css')
@section('content')
@include('include.banner')
<!-- Danh Muc -->
<div class="container ctvc mt-5">
    <h3 class="ctvc__title">
        Danh mục sản phẩm
    </h3>
    <div class="row ctvc__border">
        @if(count($category) == 0)
        Không có danh mục nào
        @else
        @foreach($category as $value_cate)
        <div class="col-md-2 col-6 col-sm-4 ctvc__item">
            <a href="{{route('getCate',['id'=>$value_cate->id])}}">
                <img src="../../public/uploads/img_category/{{$value_cate->img}}" class="avatar_cate img-fluid ctvc__img"
                    alt="">
                <div class="ctvc__item-content">
                    <a href="{{route('getCate',['id'=>$value_cate->id])}}">{{$value_cate->name}}</a>
                    <span>{{count($qty_product["".$value_cate->id.""])}} sản phẩm</span>
                </div>
            </a>
        </div>
        @endforeach
        @endif
        <div class="ctvc__item-content text-center mb-5" style="width: 100%">
            <a href="{{route('getCate',['id'=>$value_cate->id])}}">Tất cả các loại <i
                    class="fa fa-chevron-right"></i></a>
        </div>

    </div>
</div>
<!-- Product -->
<div class="container ctvp">
    <h3 class="ctvp__title">Sản phẩm Mua Nhiều </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    @if(count($product_buy) == 0)
                    Không có sản phẩm
                    @else

                    @foreach($product_buy as $promotion)
                    <div class="in-product">
                        <div class="img">
                            <img src="../../public/uploads/img_product/{{$promotion->img}}" alt="Avatar" class="image">
                            <div class="overlay">
                                <i id="{{$promotion->id}}" class="cart_index_sale">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </i>
                                @if(Auth::check())
                                    <i id="{{$promotion->id}}" class="love_index_sale">
                                        <i class="fa fa-heart group-button--icon"></i>
                                    </i>
                                @endif
                                <a href="{{ route('getDetailproduct',$promotion->id)}}">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </a>
                            </div>
                            <span>
                                @if($promotion->promotion >0)
                                giảm giá {{$promotion->promotion}} %
                                @endif
                            </span>
                        </div>
                        <h3>{{$promotion->name}}</h3>
                        @if($promotion->promotion >0)
                        <span class="price-product--cost">
                            <strike>{{number_format($promotion->price,0,'.',',')}}</strike> VNĐ
                        </span>
                        <span class="price-product--sale">
                            @if($promotion->promotion >0)
                            -
                            {{number_format($promotion->price - ($promotion->price*($promotion->promotion/100)),0,'.',',')}}
                            VNĐ
                            @endif
                        </span>
                        @else
                        <span class="price-product--cost">
                            {{number_format($promotion->price,0,'.',',')}}
                        </span>
                        @endif
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product -->
<div class="container ctvp">
    <h3 class="ctvp__title">Sản phẩm Xem Nhiều </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    @if(count($product_view) == 0)
                    Không có sản phẩm
                    @else
                    @foreach($product_view as $promotion)
                    <div class="in-product">
                        <div class="img">
                            <img src="../../public/uploads/img_product/{{$promotion->img}}" alt="Avatar" class="image">
                            <div class="overlay">
                                <i id="{{$promotion->id}}" class="cart_index_sale">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </i>
                                @if(Auth::check())
                                    <i id="{{$promotion->id}}" class="love_index_sale">
                                        <i class="fa fa-heart group-button--icon"></i>
                                    </i>
                                @endif
                                <a href="{{ route('getDetailproduct',$promotion->id)}}">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </a>
                            </div>
                            <span>
                                @if($promotion->promotion >0)
                                giảm giá {{$promotion->promotion}} %
                                @endif
                            </span>
                        </div>
                        <h3>{{$promotion->name}}</h3>
                        @if($promotion->promotion >0)
                        <span class="price-product--cost">
                            <strike>{{number_format($promotion->price,0,'.',',')}}</strike> VNĐ
                        </span>
                        <span class="price-product--sale">
                            @if($promotion->promotion >0)
                            -
                            {{number_format($promotion->price - ($promotion->price*($promotion->promotion/100)),0,'.',',')}}
                            VNĐ
                            @endif
                        </span>
                        @else
                        <span class="price-product--cost">
                            {{number_format($promotion->price,0,'.',',')}}
                        </span>
                        @endif
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>



<style type="text/css">
    .img {
    position: relative;
    }

    .image {
    display: block;
    width: 100%;
    height: 210px;
    }

    .overlay {
    position: absolute; 
    bottom: 0; 
    color: #f1f1f1; 
    width: 100%;
    transition: .5s ease;
    opacity:0;
    color: white;
    font-size: 20px;
    padding: 20px;
    text-align: center;
    }

    .img:hover .overlay {
    opacity: 1;
    }
    .overlay a{
        color: white;
    }
    .in-product{
        text-align: center;
        padding: 10px;
        border: 1px solid #eee;
    }
    .in-product h3{
        color:#323232;
        font-size:14px;
        line-height:20px;
    }
    .img span{
        position:absolute;
        top: 8px;
        left: 8px;
        background: red;
        color: #fff;
        padding: 0px 3px;
        border-radius: 5px;
        box-shadow: 1px 1px;
    }
</style>

<div class="container ctvp">
    <h3 class="ctvp__title">
        Sản phẩm Mới Nhất 
    </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    @if(count($product_created) == 0)
                    Không có sản phẩm
                    @else
                    @foreach($product_created as $promotion)
                    <div class="in-product">
                        <div class="img">
                            <img src="../../public/uploads/img_product/{{$promotion->img}}" alt="Avatar" class="image">
                            <div class="overlay">
                                <i id="{{$promotion->id}}" class="cart_index_sale">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </i>
                                @if(Auth::check())
                                    <i id="{{$promotion->id}}" class="love_index_sale">
                                        <i class="fa fa-heart group-button--icon"></i>
                                    </i>
                                @endif
                                <a href="{{ route('getDetailproduct',$promotion->id)}}">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </a>
                            </div>
                            <span>
                                @if($promotion->promotion >0)
                                giảm giá {{$promotion->promotion}} %
                                @endif
                            </span>
                        </div>
                        <h3>{{$promotion->name}}</h3>
                        @if($promotion->promotion >0)
                        <span class="price-product--cost">
                            <strike>{{number_format($promotion->price,0,'.',',')}}</strike> VNĐ
                        </span>
                        <span class="price-product--sale">
                            @if($promotion->promotion >0)
                            -
                            {{number_format($promotion->price - ($promotion->price*($promotion->promotion/100)),0,'.',',')}}
                            VNĐ
                            @endif
                        </span>
                        @else
                        <span class="price-product--cost">
                            {{number_format($promotion->price,0,'.',',')}}
                        </span>
                        @endif
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container ctvp">
    <h3 class="ctvp__title">Sản phẩm Sale Mạnh </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    @if(count($product_promotion) == 0)
                    Không có sản phẩm
                    @else   
                    @foreach($product_promotion as $promotion)
                    <div class="in-product">
                        <div class="img">
                            <img src="../../public/uploads/img_product/{{$promotion->img}}" alt="Avatar" class="image">
                            <div class="overlay">
                                <i id="{{$promotion->id}}" class="cart_index_sale">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </i>
                                @if(Auth::check())
                                    <i id="{{$promotion->id}}" class="love_index_sale">
                                        <i class="fa fa-heart group-button--icon"></i>
                                    </i>
                                @endif
                                <a href="{{ route('getDetailproduct',$promotion->id)}}">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </a>
                            </div>
                            <span>
                                @if($promotion->promotion >0)
                                giảm giá {{$promotion->promotion}} %
                                @endif
                            </span>
                        </div>
                        <h3>{{$promotion->name}}</h3>
                        @if($promotion->promotion >0)
                        <span class="price-product--cost">
                            <strike>{{number_format($promotion->price,0,'.',',')}}</strike> VNĐ
                        </span>
                        <span class="price-product--sale">
                            @if($promotion->promotion >0)
                            -
                            {{number_format($promotion->price - ($promotion->price*($promotion->promotion/100)),0,'.',',')}}
                            VNĐ
                            @endif
                        </span>
                        @else
                        <span class="price-product--cost">
                            {{number_format($promotion->price,0,'.',',')}}
                        </span>
                        @endif
                    </div>
                    @endforeach
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <!-- Product -->
>>>>>>> Stashed changes
<div class="container ctvp">
    <h3 class="ctvp__title">
        Sản phẩm Mua Nhiều
    </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    <!-- item product -->
                    <div class="pro-item">
                        <div class="img">
                            <img src="#" alt="Avatar" class="image">
                            <div class="over-btn">
                                <a href="#">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> --}}




<!-- Product -->
<div class="container ctvp">
    <h3 class="ctvp__title">
        Blog
    </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    <!-- item product -->

                    @foreach($blog as $promotion)
                    <a href="{{ route('getDetailBlog',$promotion->id) }}">
                        <div class=" item__product">
                           
                            
                            <div class="caption">
                                
                                
                            </div>
                            <img src="../../public/uploads/img_blog/{{$promotion->img}}" width="80px" height="80px" class="product__img"
                                alt="" />
                                <h3 class="name-product">{{$promotion->title}}</h3>
                                Xem chi tiết
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" />
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" />
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />

<script src="assets/js/jquery-3.5.1.min.js"></script>
<script>
    $("i.love_index_sale").click(function() {
        var id = (this.id);
        $.ajax({ 
            url: '{{route('love_index_sale')}}',
            type: 'GET',
            data: {
                id: id,
            },
            success: function (data) {
               
                alertify.success(data);     
            }
        });
    });

    $("i.cart_index_sale").click(function() {
        var id = (this.id);
        
        $.ajax({ 
            url: '{{route('cart_index_sale')}}',
            type: 'GET',
            data: {
                id: id,
            },
            success: function (data) {
                $('#count').html(data['count']);
                alertify.success(data['string']);     
            }
        });
    });
</script>

@endsection('content')
 -->