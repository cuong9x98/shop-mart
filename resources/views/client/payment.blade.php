@extends('client.master')
@section('title','Thanh Toán')
@section('content')

<style type="text/css">
    .shop-tb{
        overflow: auto;
    }
    table{
        text-align: center;
        width: 100%;
    }
    th{
        min-width: 150px;
        padding: 10px 0;
    }
    th button{
        background: black;
        color: white;
        padding: 5px 10px;
        outline: none;
    }
    .table-p{
        border: 1px solid;
    padding: 10px 10px;
    }
</style>
<?php 
    $content = Cart::content();
    $count = Cart::count();
?>
<div class="container">
<form action="{{route('order.store')}}" method="post">
@csrf
    <div class="shop-tb mt-5">
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Số lượng</th>
                <th>Giá</th>
                <th>Tổng</th>
                
            </tr>
            @foreach($content as $result)
            <tr>
                <td>{{$result->id}}</td>
                <td>{{$result->name}}</td>
                <td>{{$result->qty}}</td>
                <td>
                {{$result->price.' '.'VNĐ'}}
                </td>
                <td>{{$result->price*$result->qty}}</td>
               
            </tr>
           

            @endforeach
        </table>
    </div>
    <div class="table-p mt-5">
            <div class="table-p-content">
                <div class="row">
                    <div class="col-md-2 col-4">
                        <span><b>Thành Tiền:</b></span>
                    </div>
                    <div class="col-md-10 col-8">
                    <p>{{Cart::subtotal().'VNĐ'}}</p>
                    <input type="hidden" name="total_price" value="{{Cart::subtotal()}}">
                    <input type="hidden" name="qty" value="{{$result->qty}}">
                    <input type="hidden" name="id_prod" value="{{$result->id}}">
                    <input type="hidden" name="id_create" value="{{$result->options->creater}}">
                    <input type="hidden" name="soluong" value="{{$count}}">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-4">
                        <span><b>Tên khách:</b></span>
                    </div>
                    <div class="col-md-10 col-8">
                        <span><p>{{session('name')}}</p></span>
                        <input type="hidden" name="name" value="{{session('name')}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-4">
                        <span><b>Số Điện Thoại:</b></span>
                        
                    </div>
                    <div class="col-md-10 col-8">
                        <span><p>{{session('phone')}}</p></span>
                        <input type="hidden" name="phone" value="{{session('phone')}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-4">
                        <span><b>Email:</b></span>
                        
                    </div>
                    <div class="col-md-10 col-8">
                        <span><p>{{session('email')}}</p></span>
                        <input type="hidden" name="email" value="{{session('email')}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 col-4">
                        <span><b>Địa Chỉ</b></span>
                    </div>
                    <div class="col-md-10 col-8">
                    <span><p>{{session('city')}}-{{session('town')}}-{{session('village')}}-{{session('street')}}</p></span>   
                    <input type="hidden" name="address" value="{{session('city')}}-{{session('town')}}-{{session('village')}}-{{session('street')}}">
                    </div>
                </div>
            </div>
        </div>

    <div class="text-center mt-5 mb-5">

        <button type="submit" class="btn btn-primary">Thanh Toán</button>
        <button type="button" class="btn btn-primary">Hủy</button>
    </div>
    </form>
</div>


    <script src="asset/js/jquery.min.js" defer></script>
    <script src="asset/bootstrap/js/bootstrap.min.js" defer></script>

@endsection('content')