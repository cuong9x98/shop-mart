<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('cart');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',8);
            $table->integer('qty');
            $table->unsignedBigInteger('id_prod');
            $table->foreign('id_prod')
                ->references('id')
                ->on('product')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_creater');
            $table->foreign('id_creater')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_customer');
            $table->foreign('id_customer')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }
}
