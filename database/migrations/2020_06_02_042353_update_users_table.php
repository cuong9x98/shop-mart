<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Chạy composer require doctrine/dbal
        Schema::table('users', function (Blueprint $table) {
            $table->string('img')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->integer('sex')->nullable()->change();
            $table->string('date')->nullable()->change();
            $table->string('country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('img');
            $table->string('email')->unique();
            $table->string('address');
            $table->string('phone')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('sex');
            $table->string('date');
            $table->Integer('role');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }
}
