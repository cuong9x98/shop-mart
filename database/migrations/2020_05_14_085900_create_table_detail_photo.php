<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDetailPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_photo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('img');
            $table->unsignedBigInteger('id_prod');
            $table->foreign('id_prod')
                ->references('id')
                ->on('product')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_photo');
    }
}
